import 'package:delightful_toast/delight_toast.dart';
import 'package:delightful_toast/toast/components/toast_card.dart';
import 'package:delightful_toast/toast/utils/enums.dart';
import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _buildUI(),
    );
  }

  Widget _buildUI() {
    return SizedBox(
      width: MediaQuery.sizeOf(context).width,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          ElevatedButton(
            onPressed: () {
              ScaffoldMessenger.of(context).showSnackBar(
                const SnackBar(
                  duration: Durations.extralong3,
                  content: Text("This is a normal toast."),
                ),
              );
            },
            style: ElevatedButton.styleFrom(
              shape: const StadiumBorder(),
            ),
            child: const Text('Normal Toast'),
          ),
          ElevatedButton(
            onPressed: () {
              DelightToastBar(
                builder: (context) {
                  return const ToastCard(
                    leading: Icon(
                      Icons.notifications,
                      size: 32,
                    ),
                    title: Text(
                      "This is a delightful toast.",
                      style: TextStyle(
                        fontWeight: FontWeight.w700,
                        fontSize: 14,
                      ),
                    ),
                  );
                },
                position: DelightSnackbarPosition.top,    // toast comes at top
                autoDismiss: true,
                snackbarDuration: Durations.extralong4,
              ).show(
                context,
              );
            },
            style: ElevatedButton.styleFrom(
              shape: const StadiumBorder(),
            ),
            child: const Text('Stylized Toast'),
          )
        ],
      ),
    );
  }
}
